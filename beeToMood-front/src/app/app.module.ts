import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HeaderComponent } from './header/header.component';
import { UsersListComponent } from './users/users-list/users-list.component';
import {HttpClientModule} from '@angular/common/http';
import { UserCaseComponent } from './users/user-case/user-case.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GroupsListComponent } from './groups/groups-list/groups-list.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login/login.component';
import { LogoutComponent } from './login/logout/logout.component';
import { MoodComponent } from './mood/mood.component';
import {FormsModule} from '@angular/forms';
import { GroupAddComponent } from './groups/group-add/group-add.component';
import { ErrorComponent } from './error/error.component';
import { AdminComponent } from './admin/admin/admin.component';
import { ManagerComponent } from './manager/manager/manager.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UsersListComponent,
    UserCaseComponent,
    DashboardComponent,
    GroupsListComponent,
    FooterComponent,
    LoginComponent,
    LogoutComponent,
    MoodComponent,
    GroupAddComponent,
    ErrorComponent,
    AdminComponent,
    ManagerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
