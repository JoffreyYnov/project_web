import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersListComponent } from './users/users-list/users-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GroupsListComponent } from './groups/groups-list/groups-list.component';
import { LoginComponent } from './login/login/login.component';
import { MoodComponent } from './mood/mood.component';
import { AuthGuard } from './auth/auth.guard';
import { GroupAddComponent } from './groups/group-add/group-add.component';
import { ErrorComponent } from './error/error.component';
import {AdminComponent} from './admin/admin/admin.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'mood', component: MoodComponent, canActivate: [AuthGuard] },
  { path: 'users', component: UsersListComponent, canActivate: [AuthGuard] },
  { path: 'groups', component: GroupsListComponent, canActivate: [AuthGuard] },
  { path: 'groups/add', component: GroupAddComponent, canActivate: [AuthGuard] },
  { path: 'admin', component: AdminComponent, canActivate: [AuthGuard] },
  { path: '**', component: ErrorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
