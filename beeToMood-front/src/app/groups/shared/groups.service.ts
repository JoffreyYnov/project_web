import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('apiKey')
    })
  };

  public groups$;

  constructor(private http: HttpClient) {
    this.get();
  }

  get() {
    this.http.get(environment.server + '/groups', this.httpOptions).subscribe(res => {
      console.log(res);
      this.groups$ = res;
    });
  }

  add(name, description, hours) {
    console.log(name);
    let body = {name: name, description: description, hours: hours};
    this.http.post(environment.server + '/groups/create', body, this.httpOptions).subscribe(res => {
    });
  }

  delete(id) {
    this.http.post(environment.server + '/groups/delete/' + id, {group: id}, this.httpOptions).subscribe(
      res => {
        console.log('destroy : ' + id);
      }
    );
  }
}
