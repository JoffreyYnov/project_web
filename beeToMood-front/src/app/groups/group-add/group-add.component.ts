import {Component, OnInit} from '@angular/core';
import {GroupsService} from '../shared/groups.service';

@Component({
  selector: 'app-group-add',
  templateUrl: './group-add.component.html',
  styleUrls: ['./group-add.component.scss']
})
export class GroupAddComponent implements OnInit {

  name: string;
  description: string;
  hours: string;

  constructor(protected groupService: GroupsService) {
  }

  ngOnInit() {
  }

  add() {
    this.groupService.add(this.name, this.description, this.hours);
  }

}
