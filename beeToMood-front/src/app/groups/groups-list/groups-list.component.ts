import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {GroupsService} from '../shared/groups.service';

@Component({
  selector: 'app-groups-list',
  templateUrl: './groups-list.component.html',
  styleUrls: ['./groups-list.component.scss']
})
export class GroupsListComponent implements OnInit {

  public groups: Array<any>;

  constructor(private http: HttpClient, protected groupService: GroupsService) {

  }

  ngOnInit() {
    this.groups = this.groupService.groups$;
  }

  getGroups() {
  }

  delete(id) {
    this.groupService.delete(id);
  }

}
