import {Component, OnInit} from '@angular/core';
import {LoginService} from '../login/shared/login.service';
import {AuthService} from '../auth/auth.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  login = false;
  user;
  userSubscription: Subscription;

  constructor(protected authService: AuthService) {
    this.userSubscription = this.authService.user$.subscribe(data => {
      this.user = data;
      this.login = true;
      console.log(this.user);

      if (this.user && this.user.group === 'admin') {
        this.isAdmin = true;
        console.log('admin');
      } else if (this.user && this.user.group === 'manager') {
        this.isManager = true;
        console.log('manager');
      }

    });
  }

  public isAdmin = false;
  public isManager = false;

  ngOnInit() {
    console.log(this.user)
    if (!this.user) {
      this.login = false;
    }
  }


}
