import {OnDestroy} from '@angular/core';

export class User implements OnDestroy{
  constructor(email, group) {
    this.email = email;
    this.group = group;
  }

  username: string;
  email: string;
  group: string;

  ngOnDestroy(){

  }
}
