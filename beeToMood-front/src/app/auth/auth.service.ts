import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {User} from './user';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: User;
  public user$: Subject<User>;

  constructor(private router: Router, private http: HttpClient) {
    this.user$ = new Subject<User>();
    if (localStorage.getItem('apiKey')) {
      //this.user = new User();
    }
  }

  public isLogged() {
    return !!this.user;
  }

  public login(mail, password) {
    console.log(mail);
    this.http.post(`${environment.server}/login`, {email: mail, password: password}).toPromise().then((res) => {
      localStorage.setItem('apiKey', res['success'].token);
      console.log('logged');
      this.user = new User(res['user'].email, res['group'][0]);
      console.log(this.user);
      this.user$.next(this.user);
      this.router.navigate(['home'])
      return true;
    }, (error) => {
      console.log('erreur');
      return false;
    });
  }


  public logout() {
    localStorage.removeItem('apiKey');
    this.router.navigate(['home']);
    this.user = null;
    this.user$.next(this.user);
  }
}
