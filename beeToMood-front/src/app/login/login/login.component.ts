import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public mail: string;
  public password: string;

  constructor(protected router: Router, private authService: AuthService) {
  }

  ngOnInit() {
    this.mail = 'raquel.erdman@example.net';
    this.password = 'secret';
  }

  public login() {
    this.authService.login(this.mail, this.password);
  }

}
