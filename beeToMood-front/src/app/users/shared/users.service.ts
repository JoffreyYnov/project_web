import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('apiKey')
    })
  };

  public users$;

  constructor(private http: HttpClient) {
    //localStorage.setItem('apiKey', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU2ZmM4M2I3NWM1MThmMjY1MjlkN2UwYTI3NmM0OTQ4NTVhMjJjM2NlZTg2M2RkNGVlYmM5MzQzMTAyNTQ2MDVhYzc0NDU5ZWIxZDkzYzNhIn0.eyJhdWQiOiIxIiwianRpIjoiNTZmYzgzYjc1YzUxOGYyNjUyOWQ3ZTBhMjc2YzQ5NDg1NWEyMmMzY2VlODYzZGQ0ZWViYzkzNDMxMDI1NDYwNWFjNzQ0NTllYjFkOTNjM2EiLCJpYXQiOjE1Mjk1ODQzNDIsIm5iZiI6MTUyOTU4NDM0MiwiZXhwIjoxNTYxMTIwMzQyLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.GpiRGqYmQVZuaR5Xxt_iOxqL3yKzMo9hfgmygsvO0rFSxbvfBr1OVJ0I5F1bQ7TIrCiX0uVJMIK1Xf2sZZVnWlW60aiHUJuM9V3o2Z1QbgAbdZ_qweGQDpGv-BcaX5IbxmaFkTBP5PBVa3UI4QXJoPmCQu3w6V8HgPxMNFHQYHq5jEV95tsYyZ2eHkS65Ld24aU3nl1EbquFHhQR7n-0gP0Dx-1J9vIGWhVlZxRTBoveGU4fDdUF9XmfjLeYeimm1snprKaDm_GUI0rUM0gjzq1jCye1NnJCYXEwrQ-durUF9MKPTecpqL5JIMk1-aPvmelJj5zlphxfjFrTC9Ose9Cni5PNM_KhYGN-dzL08yVdpe5BS3GFLDi5xQm7plTs-0tKoHMa5QNbWJyOJ1IIDz6UFCtxxUAh4hVovuu3o4Fkv3Xx-KDOGI8uxeAVe68Batq6YS2s9hVkzm7YPAsv3MGZcJs96jvVaNep-SB4Sh0kULzh6ucaBDLlADKWyfY5iVLB5B_x_WsxMXpgth44rKCsT7j8K4-t1k46Um4Jng7C5J6lcvaWwmki4dHitfMuHXYhey5IZy4yyx8Rs5sCT7yRE6vZkBCNiljTMMr11ng-d7B_7-4o7sEZ-CkVsVTTdzSpmUSokx95Rm6EMs6DYPd1jXhFzA14mL3_5TjHqYQ')
    this.http.get(environment.server + '/users', this.httpOptions).subscribe(res => {
      console.log(res);
      this.users$ = res;
    });
  }

}
