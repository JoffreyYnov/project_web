import { Component, OnInit } from '@angular/core';
import { UsersService } from '../shared/users.service';


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  public users;

  constructor(private userService: UsersService) {
  }

  ngOnInit() {
    this.users = this.userService.users$;
    console.log(this.users);
  }

}
