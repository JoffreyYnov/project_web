import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mood',
  templateUrl: './mood.component.html',
  styleUrls: ['./mood.component.scss']
})
export class MoodComponent implements OnInit {

  protected mood: number;

  constructor() { }

  ngOnInit() {
  }

  setMood(mood) {
    this.mood = mood;
    console.log(mood)
  }

  // setHappy() {
  //recupere de quoi identifier, l'user requete le server avec l'emo en question
  // }

}
