<?php

namespace App\Helpers;

use App\Group;

class GroupHelper
{
    public static function get()
    {
        return Group::all();
    }
}