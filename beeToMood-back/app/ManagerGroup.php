<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use MongoDB\Driver\Manager;

class ManagerGroup extends Model
{
    protected $table = 'manager_group';
}
