<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function managers()
    {
        return $this->belongsToMany(ManagerGroup::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
