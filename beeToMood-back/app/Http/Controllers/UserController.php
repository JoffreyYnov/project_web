<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        if(Auth::user()->hasRole('admin')){
            return User::all();
        }
        else if(Auth::user()->hasRole('manager')){
            $users =  Auth::user()->manage()->with('users')->first();
            return $users->users;
        }
        else{
            return false;
        }
    }

    /**
     * Show user details
     *
     * @param Request $request
     * @return mixed
     */
    public function show(Request $request)
    {
        if(Auth::user()->hasRole('admin')){
            return User::find($request->get('id'))->first();
        }else{
            $users =  Auth::user()->manage()->with('users')->first();
            if(array_has($users->users, $request->get('id'))){
                return User::find($request->get('id'))->first();
            }
            else{
                return false;
            }
        }
    }

    /**
     * Create user
     *
     */
    public function create()
    {
    }

    /**
     * Store new user
     *
     */
    public function store(Request $request)
    {
        $user = new User($request);

    }

    public function edit()
    {
        if(Auth::user()->hasRole('admin')){
            return User::find($request->get('id'))->first();
        }else{
            $users =  Auth::user()->manage()->with('users')->first();
            if(array_has($users->users, $request->get('id'))){
                return User::find($request->get('id'))->first();
            }
            else{
                return false;
            }
        }
    }

    public function update()
    {
        
    }
}
