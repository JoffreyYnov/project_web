<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    public function permissions()
    {
        User::find(1)->assignRole('admin');
        echo 'ok';
        return 0;
    }
}
