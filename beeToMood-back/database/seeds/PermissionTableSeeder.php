<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;


class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'add emp',
            'edit emp',
            'delete emp',
            'add manager',
            'edit manager',
            'delete manager',
            'add admin',
            'edit admin',
            'delete admin',
            'add group',
            'edit group',
            'delete group',
            'associate manager',
            'associate emp',
            'dissociate manager',
            'dissociate emp',
        ];

        app(PermissionRegistrar::class)->forgetCachedPermissions();
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }


        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo($permissions);

        $role = Role::create(['name' => 'manager']);
        $role->givePermissionTo([
            'add emp',
            'edit emp',
            'delete emp',
            'add group',
            'edit group',
            'delete group',
            'associate emp',
            'dissociate emp',
        ]);


    }
}
