<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\PassportController@login')->name('login');

Route::get('setPermission', 'TestController@permissions');

Route::group(['middleware' => 'auth:api'], function(){

    //users routing
    Route::get('users', 'UserController@index')->name('users.index');
    Route::get('users/{id}', 'UserController@show')->name('users.show');
    Route::get('users/create', 'UserController@create')->name('users.create');
    Route::post('users/create', 'UserController@store')->name('users.store');
    Route::get('users/edgit/{id}', 'UserController@edit')->name('users.edit');
    Route::post('users/edit/{id}', 'UserController@update')->name('users.update');

    //team routing
    Route::get('groups', 'GroupController@index')->name('groups.index');
    Route::get('groups/{id}', 'GroupController@show')->name('groups.show');
    Route::get('groups/create', 'GroupController@create')->name('groups.create');
    Route::post('groups/create', 'GroupController@store')->name('groups.store');
    Route::get('groups/edit/{id}', 'GroupController@edit')->name('groups.edit');
    Route::post('groups/edit/{id}', 'GroupController@update')->name('groups.update');
    Route::post('groups/delete/{id}', 'GroupController@destroy')->name('groups.destroy');

    //mood routing
    Route::get('moods', 'MoodController@index')->name('moods.index');
    Route::get('moods', 'MoodController@show')->name('moods.show');
    Route::get('moods/create', 'MoodController@create')->name('moods.create');
    Route::post('moods/create', 'MoodController@store')->name('moods.store');
    Route::get('moods/edit/{id}', 'MoodController@edit')->name('moods.edit');
    Route::post('moods/edit/{id}', 'MoodController@update')->name('moods.update');

});